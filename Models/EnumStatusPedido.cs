namespace tech_test_payment_api.Models
{
    public enum EnumStatusPedido
    {
        Aguardando_Pagamento,
        Pagamento_Aprovado,
        Enviado_Para_Transportadora,
        Entregue,
        Cancelado
    }
}